#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <unordered_map>

#include "console.hpp"

class Application
{
    Console& console_;
public:
    Application(Console& console) : console_{console}
    {}

    void run()
    {
        console_.print("App");
    }    
};

#endif // APPLICATION_HPP
