#include "cpp_sorter.h"
#include <iostream>

using namespace std;

int main()
{
	int arr[10] = { 6, 5, 6, 7, 823, 34};

	array_sort(arr, 10);

	for(const auto& item : arr)
		cout << item << " ";
	cout << endl;

	return 0;
}