#ifndef SWITCH_HPP
#define SWITCH_HPP

namespace DIP
{
    class ISwitch
    {
    public:
        virtual ~ISwitch() = default;
        virtual void on()  = 0;
        virtual void off() = 0;
    };
}

#endif //SWITCH_HPP
