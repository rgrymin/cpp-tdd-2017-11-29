cmake_minimum_required(VERSION 2.8.8)
get_filename_component(ProjectId ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})

set(PROJECT_NAME_STR ${ProjectId})
project(${PROJECT_NAME_STR} C CXX)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  add_definitions(-std=c++14 -stdlib=libstdc++ -pthread)
  set(COMPILER_DEFS "-std=c++14 -stdlib=libstdc++ -pthread")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  add_definitions(-Wall -std=c++14 -pthread)
  set(COMPILER_DEFS "-std=c++14 -pthread")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  add_definitions(-D_SCL_SECURE_NO_WARNINGS)
  set(COMPILER_DEFS "-D_VARIADIC_MAX=10")
endif()

#-------------------
# Threads
#-------------------
find_package(Threads REQUIRED)

#-------------------
# set common include folder for module
#-------------------
set(COMMON_INCLUDES ${PROJECT_SOURCE_DIR}/include)
set(EXTERNAL_PROJECTS_DIR ${PROJECT_SOURCE_DIR}/external)

#-------------------
# Library
#-------------------
set(PROJECT_LIB_NAME ${PROJECT_NAME_STR}_impl)
file(GLOB SRC_FILES ${PROJECT_SOURCE_DIR}/src/*.cpp  ${PROJECT_SOURCE_DIR}/src/*.h ${PROJECT_SOURCE_DIR}/src/*.hpp)
file(GLOB SRC_HEADERS ${PROJECT_SOURCE_DIR}/include/*.h ${PROJECT_SOURCE_DIR}/include/*.hpp)
add_library(${PROJECT_LIB_NAME} ${SRC_FILES} ${SRC_HEADERS})
target_include_directories(${PROJECT_LIB_NAME} PUBLIC ${COMMON_INCLUDES})

#-------------------
# Tests
#-------------------
enable_testing()
add_subdirectory(tests)

#----------------------------------------
# Application
#----------------------------------------
set(PROJECT_NAME ${PROJECT_NAME_STR})

# Headers
file(GLOB HEADERS_LIST "*.h" "*.hpp")
include_directories(${COMMON_INCLUDES})
add_executable(${PROJECT_NAME} main.cpp ${SRC_LIST})
target_link_libraries(${PROJECT_NAME} ${PROJECT_LIB_NAME})
