#include "gtest/gtest.h"
#include "bowling_game.hpp"

class BowlingGameTests : public ::testing::Test
{
protected:
    BowlingGame game;

    void roll_many(size_t count, size_t pins)
    {
        for(size_t i = 0; i < count; ++i)
            game.roll(pins);
    }

    void roll_spare()
    {
        game.roll(1);
        game.roll(9);
    }

    void roll_strike()
    {
        game.roll(BowlingGame::pins_in_frame);
    }
};

TEST_F(BowlingGameTests, WhenGameStartsScoreIsZero)
{
    ASSERT_EQ(game.score(), 0u);
}

TEST_F(BowlingGameTests, WhenAllRollsInGutterScoreIsZero)
{
    roll_many(20, 0);

    ASSERT_EQ(game.score(), 0u);
}

TEST_F(BowlingGameTests, WhenAllRollsNoMarkScoreIsSumOfPins)
{
    roll_many(20, 2);

    ASSERT_EQ(game.score(), 40u);
}

TEST_F(BowlingGameTests, WhenSpareNextRollIsCountedTwice)
{
    roll_spare();
    roll_many(18, 1);

    ASSERT_EQ(game.score(), 29u);
}

TEST_F(BowlingGameTests, WhenStrikeTwoNextRollsAreCountedTwice)
{
    roll_many(2, 1);
    roll_strike();
    roll_many(16, 1);

    ASSERT_EQ(game.score(), 30u);
}

TEST_F(BowlingGameTests, StrikeAndSpareInAGame)
{
    roll_many(2, 1); // 2
    roll_strike(); // 10
    roll_spare(); // 10 + 10
    roll_many(14, 1); // 14 + 1

    ASSERT_EQ(game.score(), 47u);
}

TEST_F(BowlingGameTests, WhenSpareInLastFrameBonusExtraRoll)
{
    roll_many(18, 1);
    roll_spare();
    game.roll(6);

    ASSERT_EQ(game.score(), 34u);
}

TEST_F(BowlingGameTests, WhenStrikeInLastFrameBonusTwoExtraRolls)
{
    roll_many(18, 1);
    roll_strike();
    game.roll(6);
    game.roll(7);

    ASSERT_EQ(game.score(), 41u);
}

TEST_F(BowlingGameTests, WhenPefectGameScoreIs300)
{
    for(size_t i = 0; i < 12; ++i)
        roll_strike();

    ASSERT_EQ(game.score(), 300u);
}

TEST_F(BowlingGameTests, AllSparesScores190)
{
    for(size_t i = 0; i < 10; ++i)
    {
        game.roll(9);
        game.roll(1);
    }

    game.roll(9);

    ASSERT_EQ(game.score(), 190u);
}
