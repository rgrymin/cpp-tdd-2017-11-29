#ifndef BOWLING_GAME_HPP
#define BOWLING_GAME_HPP

#include <algorithm>
#include <array>

class BowlingGame
{
    std::array<size_t, 21> pins_ {};
    size_t roll_no = 0;
public:
    constexpr static size_t pins_in_frame = 10;
    constexpr static size_t number_of_frames = 10;

    size_t score() const
    {
        size_t result = 0;
        size_t roll_index = 0;

        for(size_t i = 0; i < number_of_frames; ++i)
        {
            if (is_strike(roll_index))
            {
                result += pins_in_frame + strike_bonus(roll_index);
                ++roll_index;
            }
            else
            {
                if (is_spare(roll_index))
                    result += spare_bonus(roll_index);

                result += frame_score(roll_index);

                roll_index += 2;
            }
        }

        return result;
    }

    void roll(size_t pins)
    {
        pins_[roll_no++] = pins;
    }
private:
    size_t frame_score(size_t roll_index) const
    {
        return pins_[roll_index] + pins_[roll_index + 1];
    }

    bool is_spare(size_t roll_index) const
    {
        return pins_[roll_index] + pins_[roll_index + 1] == pins_in_frame;
    }

    size_t spare_bonus(size_t roll_index) const
    {
        return pins_[roll_index + 2];
    }

    bool is_strike(size_t roll_index) const
    {
        return pins_[roll_index] == pins_in_frame;
    }

    size_t strike_bonus(size_t roll_index) const
    {
        return pins_[roll_index + 1] + pins_[roll_index + 2];
    }
};

#endif

